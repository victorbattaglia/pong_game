# Simple Pong Game in Python 3

import turtle
import os

# Global Variables

window_width = 800
window_height = 600

paddle_a_init_x_position = -350
paddle_a_init_y_position = 0

paddle_b_init_x_position = 350
paddle_b_init_y_position = 0

paddle_movement_speed = 20

ball_init_x_position = 0
ball_init_y_position = 0

ball_init_x_movement_speed = 0.2
ball_init_y_movement_speed = 0.2

player_1_score = 0
player_2_score = 0

player_1_wins = False
player_2_wins = False

paddle_a = turtle.Turtle()
paddle_b = turtle.Turtle()
ball = turtle.Turtle()

game_started = False
game_finished = False

# Window Setup

window = turtle.Screen()
window.title("Pong Game")
window.bgcolor("black")
window.setup(width=window_width, height=window_height)
window.tracer(0)

# Pen Setup

pen = turtle.Turtle()
pen.speed(0)
pen.color("white")
pen.penup()
pen.hideturtle()

def init_game():
    pen.goto(0,50)
    pen.write("Press Enter to play", align="center", font=("Courier", 24, "normal"))
    pen.goto(0, -20)
    pen.write("Player 1: Z for up, S for down", align="center", font=("Courier", 18, "normal"))
    pen.goto(0, -55)
    pen.write("Player 2: Up for up, Down for down", align="center", font=("Courier", 18, "normal"))
    pen.goto(0, -120)
    pen.write("FIRST TO 7 POINTS", align="center", font=("Courier", 18, "bold"))

init_game()

def game_start():
    global game_started
    global paddle_a
    global paddle_b
    global ball

    # Paddle Setup

    paddle_a.speed(0)
    paddle_a.shape("square")
    paddle_a.color("white")
    paddle_a.shapesize(stretch_len = 1,stretch_wid=5)
    paddle_a.penup()
    paddle_a.goto(paddle_a_init_x_position, paddle_a_init_y_position)

    paddle_b.speed(0)
    paddle_b.shape("square")
    paddle_b.color("white")
    paddle_b.shapesize(stretch_len = 1,stretch_wid=5)
    paddle_b.penup()
    paddle_b.goto(paddle_b_init_x_position, paddle_b_init_y_position)

    # Ball Setup

    ball.speed(0)
    ball.shape("square")
    ball.color("white")
    ball.penup()
    ball.goto(ball_init_x_position, ball_init_y_position)
    ball.dx = ball_init_x_movement_speed
    ball.dy = ball_init_y_movement_speed

    # Score Setup

    pen.clear()
    pen.goto(0, 260)
    pen.write("Player 1 : %s  Player 2 : %s" % (player_1_score, player_2_score), align="center", font=("Courier", 18, "normal"))

    game_started = True

# Move Function

def paddle_a_up():
    y = paddle_a.ycor()
    y += paddle_movement_speed
    paddle_a.sety(y)

def paddle_a_down():
    y = paddle_a.ycor()
    y -= paddle_movement_speed
    paddle_a.sety(y)

def paddle_b_up():
    y = paddle_b.ycor()
    y += paddle_movement_speed
    paddle_b.sety(y)

def paddle_b_down():
    y = paddle_b.ycor()
    y -= paddle_movement_speed
    paddle_b.sety(y)

def moving_ball():
    ball.setx(ball.xcor() + ball.dx)
    ball.sety(ball.ycor() + ball.dy)

# Border Collision

def ball_border_collision():
    global player_1_score
    global player_2_score

    if ball.ycor() > 290:
        ball.sety(290)
        ball.dy *= -1
        os.system("aplay border_bounce.wav&")

    elif ball.ycor() < -290:
        ball.sety(-290)
        ball.dy *= -1
        os.system("aplay border_bounce.wav&")

    elif ball.xcor() > 390:
        ball.goto(0,0)
        ball.dx *= -1
        player_1_score += 1
        score_update()
        os.system("aplay score.wav&")

    elif ball.xcor() < -390:
        ball.goto(0,0)
        ball.dx *= -1
        player_2_score += 1
        score_update()
        os.system("aplay score.wav&")
    
def paddle_border_collision():
    if paddle_a.ycor() > 250:
        paddle_a.sety(250)
    
    if paddle_a.ycor() < -250:
        paddle_a.sety(-250)
    
    if paddle_b.ycor() > 250:
        paddle_b.sety(250)

    if paddle_b.ycor() < -250:
        paddle_b.sety(-250)

# Paddle Collision

def ball_paddle_collision():
    if (ball.xcor() > 340 and ball.xcor() < 350) and (ball.ycor() < paddle_b.ycor() + 40 and ball.ycor() > paddle_b.ycor() - 40):
        ball.setx(340)
        ball.dx *= -1
        os.system("aplay paddle_bounce.wav&")

    elif (ball.xcor() < -340 and ball.xcor() > -350) and (ball.ycor() < paddle_a.ycor() + 40 and ball.ycor() > paddle_a.ycor() - 40):
        ball.setx(-340)
        ball.dx *= -1
        os.system("aplay paddle_bounce.wav&")
        

# Score Update

def score_update():
    pen.clear()
    pen.write("Player 1 : %s  Player 2 : %s" % (player_1_score, player_2_score), align="center", font=("Courier", 18, "normal"))

# Game Over

def game_over():
    global paddle_a
    global paddle_b
    global ball
    global player_1_wins
    global player_2_wins
    global player_1_score
    global player_2_score
    global game_started
    global game_finished

    if player_1_score == 7 or player_2_score == 7:
        game_finished = True
        
    if player_1_score == 7:
        player_1_wins = True   
    elif player_2_score == 7:
        player_2_wins = True

    if game_finished:
        paddle_a.clear()
        paddle_b.clear()
        ball.clear()
        pen.clear()
        pen.goto(0,150)

        if player_1_wins:
            pen.write("Player 1 wins !", align="center", font=("Courier", 24, "normal"))
        elif player_2_wins:
            pen.write("Player 2 wins !", align="center", font=("Courier", 24, "normal"))

        pen.goto(0, -150)
        pen.write("Press Enter to play again", align="center", font=("Courier", 18, "normal"))
        player_1_score = 0
        player_2_score = 0
        player_1_wins = False
        player_2_wins = False
        game_started = False
        game_finished = False



# Keyboard binding

window.listen()
window.onkey(paddle_a_up, "z")
window.onkey(paddle_a_down, "s")
window.onkey(paddle_b_up, "Up")
window.onkey(paddle_b_down, "Down")
window.onkey(game_start, "Return")

# Main Game loop
while True:
    window.update()
    while game_started:
        window.update()
        moving_ball()
        ball_border_collision()
        ball_paddle_collision()
        paddle_border_collision()
        game_over()