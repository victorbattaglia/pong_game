# Pong Game :joystick:

### Prérequis
- Python 3

### Lancer le jeu
#### Sous Linux

- Ouvrir un terminal
- Aller dans le dossier contenant le fichier pong.py avec la commande cd
- Taper la commande suivante `python pong.py`
